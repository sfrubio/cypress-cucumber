import { Given, Then } from "cypress-cucumber-preprocessor/steps";

const url = 'https://google.com'
Given('o acesso a página inicial do Google', () => {
  cy.visit(url)
})

Then(`a palavra {string} é apresentada no título da página`, (title) => {
  cy.title().should('include', title)
})
