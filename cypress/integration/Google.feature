
#language: pt
Funcionalidade: Página incial do Google
  Como usuário, quero acessar a página inicial do Google

  @focus
  Cenário: Abrir a página inicial do Google
    Dado o acesso a página inicial do Google
    Então a palavra "Google" é apresentada no título da página
